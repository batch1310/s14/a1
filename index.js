console.log(`Hello World`);

let fname = "Marc",
 	lname = "Smith",
	age = "30",
	isMarried = true,
	hobbies = [`Biking`, ` Mountain Climbing`, ` Swimming`];

let workAddress = {
	houseNumber: "32",
	street: "Washington",
	city: " Lincoln",
	state: " Nebraska"
}

function printUserInfo(firstName, lastName, userAge) {
	console.log(`First Name: ${firstName}`);
	console.log(`Last Name: ${lastName}`);
	console.log(`Age: ${userAge}`);

	console.log(`Hobbies: `);
	console.log(hobbies);

	console.log(`Work Address: `);
	console.log(workAddress);

	console.log(`${firstName} ${lastName} is ${userAge} years of age.`);

	console.log(`This was printed inside printUserInfo function:`);
	console.log(hobbies);

	console.log(`This was printed inside printUserInfo function:`);
	console.log(workAddress);
	console.log(`The value of isMarried is: ${isMarried}`);
}
printUserInfo(fname, lname, age);